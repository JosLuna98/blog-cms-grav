<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/user/config/site.yaml',
    'modified' => 1547505295,
    'data' => [
        'title' => 'CMS Blog Jose',
        'description' => 'Este es mi blog hecho en 2 horas',
        'taxonomies' => [
            0 => 'tag',
            1 => 'featured'
        ],
        'metadata' => [
            'description' => 'Grav is an easy to use, yet powerful, open source flat-file CMS'
        ],
        'logo' => '/user/images/logo.jpg',
        'date_long' => 'd F Y',
        'date_short' => 'd M Y',
        'author' => [
            'name' => 'Jose Luna',
            'email' => 'josluna1098@gmail.com',
            'image' => '/user/images/avatar.jpg',
            'bio' => 'Soy un estudiante de Sistemas, luchando por su nota'
        ],
        'social' => [
            0 => [
                'icon' => 'facebook',
                'url' => 'https://facebook.com/???',
                'desc' => 'Connect with me facebook',
                'share_url' => '//www.facebook.com/sharer.php',
                'share_title' => '?t=',
                'share_link' => 'https://www.facebook.com/profile.php?id=100000704533847&ref=bookmarks'
            ],
            1 => [
                'icon' => 'github',
                'url' => 'https://github.com/getgrav',
                'desc' => 'Fork me on github',
                'share_url' => NULL,
                'share_title' => NULL,
                'share_link' => 'https://JosLuna98@bitbucket.org/JosLuna98/blog-cms-grav.git'
            ]
        ]
    ]
];
