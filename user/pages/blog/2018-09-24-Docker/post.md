---
title:  "Docker"
slug: docker
date:   09/24/2018
taxonomy:
    tag: [theme, grav, installation]
image: docker-logo.png
---
#Docker


>Compré un servidor linux en DigitalOcean.com en el cual instalé Docker y desarrollé la practica del profesor que trataba de poner WordPress en un servidor Apache. Hecho esto empezé a levantar mis servidores, configuré un servidor Apache, otro MySQL y otro con PhpMyAdmin para poder subir una web desde mi repositorio en Bitbucket.
