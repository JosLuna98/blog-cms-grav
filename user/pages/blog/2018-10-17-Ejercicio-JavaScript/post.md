---
title:  "Ejercicio JavaScript"
slug: ejercicio-javascript
date:   10/17/2018
taxonomy:
    tag: [theme, grav, installation]
image: js.png
---
#Ejercicio

#What Can JavaScript Do?

<p id="demo">JavaScript can change HTML content.</p>
<button type="button" onclick="cambiar();">Click Me!</button>

<script>var a = document.getElementById("demo");function cambiar(){    if(a.textContent==="Hello JavaScript!")        a.innerHTML = "JavaScript can change HTML content.";    else {        a.innerHTML = "Hello JavaScript!";    }}</script>